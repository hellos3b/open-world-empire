
from flask import Flask, jsonify, request, render_template
from jinja2 import Template
import json, os, urllib2
import math
import MySQLdb
import MySQLdb.cursors
import urllib2
import glob

app = Flask(__name__)

#OKAY REMEMBER TO db.begin() BEFORE SELECTING, AND
#db.commit() AFTER EXECUTING OTHER WISE FUCK YOUR LIFE
#db = MySQLdb.connect(host="localhost", user="root", passwd="trapmusic", db="pubsharks",cursorclass=MySQLdb.cursors.DictCursor)
#db.autocommit(True)

@app.route("/")
def index():
    allScripts = [os.path.join(dirpath, f)
        for dirpath, dirnames, files in os.walk("./static/")
        for f in files if f.endswith('.js')]

    return render_template('/index.html',scripts=allScripts)

@app.route("/getWorld")
def getWorld():
    result = urllib2.urlopen("http://localhost:1337/getWorld").read()
    return result


if __name__ == "__main__":
	app.debug = True
	app.run(host='0.0.0.0')
