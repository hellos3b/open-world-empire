var AudioHelper = {
  load: function() {
    this.punch_swing = new Audio();
    this.punch_swing.src = "static/game/audio/punch-swing.mp3";
    this.punch_hit = new Audio();
    this.punch_hit.src = "static/game/audio/punch-contact.mp3";
    this.chicken = new Audio();
    this.chicken.src = "static/game/audio/chicken.wav"; 
    this.fire = new Audio();
    this.fire.src = "static/game/audio/fire_simple.wav";
  }
};
