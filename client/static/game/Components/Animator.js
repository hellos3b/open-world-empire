/* Animator Component
   draws sprite sheets */
Components.Animator = function(properties, Entity) {
   this.id = game.nextId();
   this.entity = Entity;   
  
   /* Sprite sheet */ 
   var spriteSheet = this.spriteSheet = new Image();
   spriteSheet.src = properties.src;

   /* mapping of all animation positions */
   this.animations = properties.animations;

   /* properties and defaults */
   this.fps = properties.fps  || 3;
   this.w = properties.width  || 32;
   this.h = properties.height || 32;
   this.tick = 0;
   /* in case needed to offset */
   this.offx = properties.offx || 0;
   this.offy = properties.offy || 0;
   /* if animation only exists for one instance */
   this.removeAfter = properties.removeAfter || false;
   this.remove = false;

   /* set default animation if exists */
   this.currentAnimation = this.animations.default;
   this.currentAnimation.frame = 0;
   this.currentAnimation.name = "default";

    /* Add to game */
    game.addRenderer( this );
    game.addUpdateComponent( this );
    
    /* Plays animation */
    this.play = function(animationName, force) {
       if (!this.currentAnimation.finish || force) {
         if (this.currentAnimation.name != animationName && this.animations[animationName]) {
              this.setAnim(animationName);
          }   
       }
    };

    this.setAnim = function(animationName) {
        this.currentAnimation = this.animations[animationName];
        this.currentAnimation.name = animationName;
        this.currentAnimation.frame = 0;

        this.tick = 0;
    };
   

    /* returns next frame */
    this.nextFrame = function() {
        var source = this.currentAnimation.source;
        var target = this.currentAnimation.target;
        var frame  = this.currentAnimation.frame;

        return (source + frame >= target) ? this.finishAnimation() : frame + 1;
    };

    /* When an animation is done */
    this.finishAnimation = function() {
         if (this.currentAnimation.onfinish) {
            this.currentAnimation.onfinish();      
         } 
         if (this.removeAfter) {
              this.remove = true;
         } else {
           if (!this.currentAnimation.loop) {
              this.play("default", true);     
           }
           return 0;
         }
    };

    /* Update animation */
    this.update = function() {
       this.tick++;
       if (this.tick == this.fps) {
            this.currentAnimation.frame = this.nextFrame();
            this.tick = 0;
        } 

    };
    /* and of course Draw! */
    this.draw = function(ctx) {
        var x = this.entity.getX() + this.offx;
        var y = this.entity.getY() + this.offy;
        var frame = this.w * (this.currentAnimation.source + 
                        this.currentAnimation.frame);

        var row = (typeof this.currentAnimation.row == "number") ?
                this.currentAnimation.row
              : this.currentAnimation.row(this.entity); 
        row  *= this.h;
        
        ctx.drawImage(this.spriteSheet, frame, row, this.w, this.h, x, y, this.w, this.h);
    };
};

Components.Animator.prototype.broadcast = function(message) {
    if (typeof message == "string") {
      if (this.animations[message]) {
          this.play(message);
      }   
    }
};
