/* Physics Component
   handles: Position, velocity, direction
*/
Components.Physics = function(properties, Entity) {
    this.id = game.nextId();
    var entity = this.entity = Entity;
    var self = this;

    /* location */
    var x = properties.x || 0;
    var y = properties.y || 0;    
    this.position = new Vec2(x, y);
    this.lastPosition = new Vec2(x, y);

    /* movement */
    this.velocity = properties.velocity || 0;
    this.dir = properties.direction || new Vec2(0.00, 0.00);
    this.dirS = this.dir.x;
    this.friction = properties.friction || false;

    /* things other components definitely need be gettable */
    entity.getX = function() { return self.position.x; };
    entity.getY = function() { return self.position.y; }; 
    entity.getDirection = function() { return (self.dirS > 0) ? 1 : -1; };

    /* movement controlled from outside */
    entity.setVelocity = function(v) { self.velocity = v; };
    entity.setXDirection = function(v) { self.dir.x = v; };
    entity.setYDirection = function(v) { self.dir.y = v; };
    entity.setDirection = function(vector) { self.dir = vector; };

    /* Add updatable */
    game.addUpdateComponent(self);

    /* Update physics */
    this.update = function() {
       /* If velocity, move object */
       if (this.velocity !== 0) {
          var dir = this.dir.normalize();      
          this.lastPosition.x = this.position.x;
          this.lastPosition.y = this.position.y;
          this.position = this.position.addV( dir.mulS( this.velocity ));
          this.entity.broadcast("move");
       }

       /* Remove velocity if there's no force pushing it */
       if (this.friction) {
            if (this.velocity > 0)
              this.velocity -= 0.6;

            if (this.velocity > 0 && this.velocity < 0.8) {
              this.velocity = 0;
              this.entity.broadcast("stop");
            }

            var x = this.dir.x, y = this.dir.y;
            var xsign = (x > 0) ? 1 : -1;
            var ysign = (y > 0) ? 1 : -1;

            this.dir.x = (Math.abs(x) > 0.25) ? this.dir.x - xsign*0.4 : xsign*0.00;
            this.dir.y = (Math.abs(y) > 0.45) ? this.dir.y - ysign*0.4 : ysign*0.00;
        }

        if (this.dir.x !== 0) {
            this.dirS = this.dir.x;
        }
    };

    /* Revert position, if walked into a wall or something */
    this.checkCollision = function(collideWith) {
        this.position.x = this.lastPosition.x;
        this.position.y = this.lastPosition.y;
        this.velocity = 0.1;
        this.broadcast("stop");
    };
};

Components.Physics.prototype.broadcast = function(message) {
    if (typeof message == "string") {
        
    } else {
        for (var i in message) {
            switch (i) {
                case "collide":
                  var collideWith = message[i];
                  this.checkCollision(collideWith);
                  break;
            }
        }
    } 
};
