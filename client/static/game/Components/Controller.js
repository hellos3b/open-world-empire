/* Handle user input */
/* dependencies: PhysicsComponent */
Components.Controller = function(properties, Entity) {
   this.id = game.nextId();
   this.entity = Entity;   
   this.speed = 4;
   // Sometimes you just need to stop input m8
   this.lock = false;

   // Yep!
   game.addUpdateComponent( this );

   this.update = function() {
      if (!this.lock) {
        var entity = this.entity;
        var speed = this.speed;

        var x = 0;
        var y = 0;
        x += Keyboard._RIGHT;
        x -= Keyboard._LEFT;
        y += Keyboard._DOWN;
        y -= Keyboard._UP;


        if (x) {
           entity.setVelocity(speed);
           entity.setXDirection(x);
           entity.broadcast("run");
        }

        if (y) {
           entity.setVelocity(speed);
           entity.setYDirection(y);
           entity.broadcast("run");
        } 

        if (Keyboard.SPACE()) {
           entity.broadcast("punch");
           this.freeze(9);
        }

        if (Keyboard.FIRST()) {
           var fireball = game.addEntity({
               "physics": {
                    x: this.entity.getX(),
                    y: this.entity.getY(),
                    velocity: 6,
                    direction: new Vec2(this.entity.getDirection(), 0)
                },
                "animator": {
                    src: "static/game/images/effects/fireball_1.png",
                    width: 32,
                    height: 32,
                    animations: {
                        default: {
                            source: 0,
                            target: 3,
                            row: 0,
                            loop: true
                        }
                    }
                }
           });
           this.entity.addComponent({
              "animator": {
                  src: "static/game/images/effects/player_blast_red.png",
                  width: 64,
                  height: 32,
                  offx: -16,
                  removeAfter: true,
                  animations: {
                      default: {
                          source: 0,
                          target: 6,
                          row: 0,
                          loop: false
                      }
                  }
              }
          });

           new Components.Timer({
              duration: 60,
              parent: fireball,
              ondone: function(props) {
                  props.parent.kill();
              }
            });

          this.entity.broadcast("cast");
        }
      }
   };

   // Freeze game for x ticks
   // useful for animations to disable input
   this.freeze = function(x) {
      var self = this;
      
      self.broadcast("freeze");

      new Components.Timer({
          duration: 6,
          component: self,
          ondone: function(props) {
              props.component.entity.broadcast("unfreeze");
          }
        });   
    };
};

Components.Controller.prototype.broadcast = function(message) {
  if (typeof message == "string") {
    switch (message) {
        case "freeze":
          this.lock = true;
          break;
        case "unfreeze":
          this.lock = false;
          break;
    }
  } else {
      for (var i in message) {
          switch (i) {
              case "collide":
                console.log("COLLISION");
                console.log(message[i]);
                break;
          }
      }
  }
};
