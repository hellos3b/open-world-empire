/* Renderer Component
   draws basic sprite
*/
Components.Renderer = function(properties, Entity) {
    this.id = game.nextId();
    this.entity = Entity; 

    var sprite = this.sprite = new Image();
    sprite.src = properties.src;

    var self = this;
    game.addRenderer( self );

    /* Draw sprite */
    this.draw = function(ctx) {
       var x = this.entity.getX();
       var y = this.entity.getY();
       ctx.drawImage(this.sprite, x, y); 
       //ctx.drawImage(this.sprite, frame, row, this.w, this.h, x, y, this.w, this.h);
    };
};

Components.Renderer.prototype.broadcast = function(message) {
    
};
