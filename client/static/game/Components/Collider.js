/* Handles collisions, puts out messages etc */
/* Colliders require a PhysicsComponent */
Components.Collider = function(properties, Entity) {
    this.id = game.nextId();
    var entity = this.entity = Entity;
    
    /* Collider properties */
    this.w = properties.width;
    this.h = properties.height;
    this.type = properties.type;

    /* register yourself */
    game.addCollider( this );

    /* continuous check */
    if (properties.continuous) {
      game.addUpdateComponent( this );
    }
    
    /* returns all objects it's colliding with in world */
    this.testWorld = function() {
        var self = this;
        var collides = this.collides;
        var collisions = game.getColliders()
          .filter(function(n) {
              if (n.id != self.id) {
                return self.collides(n.getBounds());
              }
          })
          .map(function(n) {
              self.entity.broadcast({"collide": n.type}); 
          });
    };
    
    /* Checks individual bounds */
    this.collides = function(b) {
       var bounds = this.getBounds(); 
       return bounds.intersects(b);
    };

    /* Get bounds */
    this.getBounds = function() {
        var self = this;
        return new Bounds(self.entity.getX(), self.entity.getY(), this.w, this.h);
    };

    /* Update collision */
    this.update = function() {
        this.testWorld();
    };
};

Components.Collider.prototype.broadcast = function(message) {
    if (typeof message == "string") {
      switch (message) {
          case "run":
            this.testWorld();
            break;
      }
    }
};
