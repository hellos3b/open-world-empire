/* TimerComponent
  executes a function for x ticks,
  calls step() on each tick,
  and ondone() when finished !*/
/* seperate from entities, they just hold a reference */

  Components.Timer = function(properties) {
     this.id = game.nextId();
     this.duration = properties.duration;
     this.step = properties.step || function() {};
     this.ondone = properties.ondone || function() {};
     this.tick = 0;
     this.done = false;

     /* Update tick and execute */
     this.update = function() {
        if (this.tick == this.duration) {
            this.done = true;
            this.ondone(properties);
        } else {
            this.step(properties);
            this.tick ++;
        } 

        return this;
     };

     game.addTimerComponent( this );
  };
