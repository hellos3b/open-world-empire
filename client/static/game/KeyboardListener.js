var Keyboard = {
    _LEFT: false,
    _RIGHT: false,
    _UP: false,
    _DOWN: false,
    _SPACE: false,
    SPACE: function() {
        if (this._SPACE) {
            this._SPACE = false;
            return true;
        }
        return false;
    },
    _FIRST: false,
    FIRST: function() {
        if (this._FIRST) {
            this._FIRST = false;
            return true;
        }
        return false;
    },
    _SECOND: false,
    init: function() {
        var self = this;
        document.onkeydown = function(e) {
            switch (e.keyCode) {
                case 68:
                   self._RIGHT = true;
                   break;
                case 65:
                   self._LEFT = true;
                   break;
                case 87:
                  self._UP = true;
                  break;
                case 83:
                    self._DOWN = true;
                  break;
            }
        };
        document.onkeyup = function(e) {
            switch (e.keyCode) {
                case 68:
                   self._RIGHT = false;
                   break;
                case 65:
                   self._LEFT = false;
                   break;
                case 87:
                  self._UP = false;
                  break;
                case 83:
                  self._DOWN = false;
                  break;
                case 32:
                  self._SPACE = true;
                  break;
                case 49:
                  self._FIRST = true;
                  break;
                case 50:
                  self._SECOND = true;
                  break;
                case 13:
                  messageHandler.openPrompt();
                  break;
            }
        };
    }
};
