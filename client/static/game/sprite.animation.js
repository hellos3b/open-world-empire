var SpriteAnimator = function(src) {
   // speed of aim
   this.fps = 3;
   // current animation tick
   this.tick = 0;
   // width of sprite
   this.w = 32;
   // height of sprite
   this.h = 32;
   // sprite Image obj
   if (typeof src == "string") {
       this.sprite = new Image(); 
      this.sprite.src = src;
   } else {
      this.sprite = src;  
   }

   this.direction = 1;
   this.default = {
       frames: [0, 0],
       row: 0
     }; 
   this.anim = this.default;
   this.animFinish = function() {};
   this.anim.current = 0;
   this.anim.currentName = "default";

   this.play = function(anim, fn) {
      if (!(anim.frames[0] == this.anim.frames[0] && anim.row == this.anim.row
            && (anim.frames[1]) == this.anim.frames[1])) {
        this.anim.frames = anim.frames;
        this.anim.row = anim.row; 
        // current frame
        this.anim.current = 0;   
        this.anim.onDone = anim.onDone;
      }
   };

   this.nextSprite = function() {
      var start = this.anim.frames[0];
      var fin = this.anim.frames[1];
      var current = this.anim.current + start;
      (current >= fin) ? this.finishAnim() : this.anim.current = this.anim.current+1;
    };

   this.finishAnim = function() {
       this.anim.current = 0;
       if (this.anim.onDone) {
            console.log("ANIMATION DONE");
            this.anim.onDone(this);
        }
    };
   this.update = function() {
      this.tick++;
      if (this.tick == this.fps) {
         this.nextSprite();
         this.tick = 0; 
      }   
   }; 

   this.draw = function(ctx, x, y) {
       this.update();
       var frame = this.w * (this.anim.frames[0] + this.anim.current);
       var row = this.h * this.anim.row;
       ctx.drawImage(this.sprite, frame, row, this.w, this.h, x, y, this.w, this.h);

    };
};
