var messageHandler = {
    ids: 0,
    nextID: function() { return this.ids++; },
    remove: function(id) {
        game.gameObjects = game.gameObjects.filter(function(n) {
           return (n.constructor.name == "message") ? n.id != id : true;
        });
    },
    inMsg: function(data) {
      game.gameObjects.filter(function(n) {
            return (n.constructor.name == "Player") ? n.id == data.fromid : false;
        }).map(function(n) {
            console.log(n);
            game.chat.push(new message(n, data.msg));
        });
    },
    sendMsg: function(msg) {
      networkManager.send({action: "speak", msg: msg});
      //game.gameObjects.push(new message(game.gameObjects[0], msg));   
    },
    promptIsOpen: false,
    openPrompt: function() {
       var p = $("#gui-messages .prompt");
       if (p.hasClass('open')) {
          var msg = p.find("input").val();
          p.find("input").val("");
          this.sendMsg(msg);
          p.removeClass('open'); 
          keys.disable = false;
       } else {
           keys.disable = true;
           p.addClass('open')
            .find('input')
            .focus();
        } 
    }
};
function message(ref, msg) {
    var self = this;
    this.id = messageHandler.nextID();

    var reference = this.reference = ref; 

    var elem = this.elem = $("<div class='message-container'><div class='message'><span class='message-text'></div></div></div>")
      .find(".message-text")
      .text(msg)
      .parents(".message-container")
      .appendTo("#gui-messages")
      .delay(3000)
      .fadeOut(1000, function() {
         messageHandler.remove(self.id);
      });

    this.topPosition = function() {
        return game.offset.offy(this.reference.bounds.y - this.elem.height() - 10);
    };
    this.leftPosition = function() {
        return game.offset.offx(this.reference.bounds.x - (this.elem.width() / 2) + 16);
    };

    this.update = function() {
      var self = this;
      elem.css({"top": self.topPosition(), "left": self.leftPosition()});
    };

    this.update();

    this.draw = function() {console.log('doodle');};
}
