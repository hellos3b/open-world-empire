/* Base entity object */
/* add shit together to make it work */
function EntityObject() {
    this.alive = true;
    this.components = [];
    
    this.addComponent = function(properties) {
       for (var i in properties) {
          var component = this.createComponent(i, properties[i]);     
          if (component)
            this.components.push(component);  
       }
    };    

    this.removeComponent = function(component) {
       this.components = this.components.filter(function(n) {
            return component.id !== n.id;
        }); 
       return false;
    };

    /* Sends message to all components */
    this.broadcast = function(msg) {
       this.components.map(function(n) { 
             n.broadcast(msg); 
          });
    };

    /* set alive to false :( */
    this.kill = function() { this.alive = false; };
}


EntityObject.prototype.createComponent = function(name, properties) {
    var self = this;
    switch(name) {
       case "physics":
          return new Components.Physics(properties, self);
       case "renderer":
          return new Components.Renderer(properties, self);
       case "animator":
          return new Components.Animator(properties, self);
       case "controller":
          return new Components.Controller(null, self);
       case "collider":
          return new Components.Collider(properties, self);
    }
    console.warn("Component not found: "+name);
    return null;
};
