game.init = function() {
    window.player = game.addEntity({
          "physics": {
              x: 150,
              y: 100,
              friction: true
          },
          "animator": {
              src: "static/game/images/player.png",
              width: 32,
              height: 32,
              animations: {
                  default: {
                      source: 0,
                      target: 0,
                      row: getRow,
                      loop: true
                  },
                  stop: {
                      source: 0,
                      target: 0,
                      row: getRow,
                      loop: true
                  },
                  run: {
                      source: 1,
                      target: 4,
                      row: getRow,
                      loop: true
                  },
                  punch: {
                      source: 5,
                      target: 7,
                      row: getRow,
                      loop: false,
                      fps: 2,
                      finish: true
                  },
                  cast: {
                      source: 5,
                      target: 7,
                      row: getRow,
                      loop: false,
                      fps: 2,
                      finish: true
                  }
              }
          },
          "collider": {
              type: "player",
              width: 32,
              height: 32
          },
          "controller": true
      });

    Keyboard.init();

    game.addEntity({
        "physics": {
            x: 200,
            y: 100
        },
        "renderer": {
            src: "static/game/images/world/grass.png",
            width: 32,
            height: 32 
        },
        "collider": {
           type: "world-wall",
           width: 32,
           height: 32 
        }
    });
};

function getRow(entity) { return (entity.getDirection() < 0) ? 0 : 1; }
