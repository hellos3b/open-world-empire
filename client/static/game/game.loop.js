game.update = function() {
    this.updateTimerComponents();

    /* clean up dead entities */
    this.entities = this.entities.filter(function(n) {
          return n.alive;
      });

    /* clean up all components of dead entities */
    this.components.updates = this.getUpdateComponents()
        .filter(game.removeDeadComponents)
        .filter(game.removeComponents);

    this.components.render = this.getRenderers()
        .filter(game.removeDeadComponents)
        .filter(game.removeComponents);


    /* update all entities that need it */
    this.getUpdateComponents().map(function(n) {
            n.update();
        });


    /* Draw */
    var ctx = Canvas.ctx;
    Canvas.clear();

    this.getRenderers().map(function(n) {
          n.draw(ctx);
      });

};

game.removeDeadComponents = function(n) { return n.entity.alive; };
game.removeComponents = function(n) { 
    return (n.remove) ? n.entity.removeComponent(n)  : true;
};
