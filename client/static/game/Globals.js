var Canvas = {};

var game   = {
    entities: [],
    components: {
        render: [],
        updates: [],
        colliders: []
    },
    timers: [],
    ids: 0
};

var Components = {};
