function Player(param) {
    this.type = "Player";
    this.name = "bob";
    this.id = -1;
    this.bounds = new Bounds(0, 0, 32, 32);

    this.sprite = new PlayerSprite(param.hair, param.shirt);
    this.wasMoving = false;
    this.acting = false;
    this.action = {};

    this.Load = function() {
        
    };

    this.update = function(inInput) {
       var self = this;
       var input;
       if (!(this.acting && this.id == networkManager.id)) {
         if (this.id == networkManager.id) {
            input = new Uint8Array(7);
            input[keys.UP] = keys._UP;
            input[keys.LEFT] = keys._LEFT;
            input[keys.DOWN] = keys._DOWN;
            input[keys.RIGHT] = keys._RIGHT;
            input[keys.SPACE] = keys._SPACE;
         } else {
            input = inInput;  
         }


      if (input) {
         var updated = false;
         // repeaters
         if (input[keys.LEFT]) {
              updated = true;
              this.wasMoving = true;
              this.sprite.left();
              this.sprite.play("run");
              var checkBounds = this.bounds.clone();
              checkBounds.x -= 3;
              checkBounds.h = 10;
              checkBounds.y += 22;
              if (!this.checkWalkCollision(checkBounds)) {
                this.bounds.x -= 3;
              } else {
                input[keys.LEFT] = false;
              }
          }
          if (input[keys.RIGHT]) {
              updated = true;
              this.wasMoving = true;
              this.sprite.right();
              this.sprite.play("run");
              var checkBounds = this.bounds.clone();
              checkBounds.x += 3;
              checkBounds.h = 10;
              checkBounds.y += 22;
              if (!this.checkWalkCollision(checkBounds)){ 
                  this.bounds.x += 3;
              } else {
                  input[keys.RIGHT] = false;
              }
          }
          if (input[keys.UP]) {
            updated = true;
            this.wasMoving = true;
            this.sprite.play("run");
            this.bounds.y -= 3;
          } 
          if (input[keys.DOWN]) {
              updated = true;
              this.wasMoving = true;
              this.sprite.play("run");
              this.bounds.y += 3;
          }
          if (!this.sprite.animating &&
               !input[keys.UP] && !input[keys.DOWN] && !input[keys.LEFT] && !input[keys.RIGHT]) {
              this.sprite.play('stand');  
          }

          // Single event
          if (keys._FIRST) {
              updated = true;
              keys._FIRST = false;
              this.acting = true; 
              this.initSkill(1);
          }
          if (keys._SECOND) {
              updated = true;
              keys._SECOND = false;
              this.acting = true;
              this.initSkill(2);
          }

          if (input[keys.SPACE]) {
              updated = true;
              this.sprite.play("punch", function() { self.sprite.play("stand"); self.acting = false; });;
              this.acting = "punch";
              this.action = new this.punch(this);
              keys._SPACE = false;
              AudioHelper.punch_swing.play();
          }

          if (this.id == networkManager.id && updated) {
              input[0] = networkManager.INPUT;
              networkManager.ws.send(input);
          } else if (this.id == networkManager.id && this.wasMoving) {
              input[0] = networkManager.INPUT;
              networkManager.ws.send(input);
              this.wasMoving = false;
          }
         }
        }
        if (this.acting == "punch") {
          this.action.update();
        }
    };
    this.initSkill = function(skillid) {
        var self = this;
        var dir = (self.sprite.direction === 0) ? -1 : 1;
        playerSkills.getSkill(skillid)(self, dir, -1);
    };
    this.execSkill = function(skilltype, dir, skillid) {
        var self = this;
        
        playerSkills.getSkill(skilltype)(self, dir, skillid);
    };
    this.checkWalkCollision = function(bounds) {
        var result = game.gameObjects.filter(function(n) { 
          return (n.solid) ?
              n.bounds.intersects(bounds)
              : false; 
        });
        return (result.length > 0) ? true : false;
    };

    this.draw = function(ctx) {
      var x = game.offset.offx(this.bounds.x);
      var y = game.offset.offy(this.bounds.y);

      this.sprite.draw(ctx, x, y);
      ctx.fillStyle = "#000000";
      ctx.textAlign="center"; 
      ctx.font="11px Arial";
      ctx.fillText(this.name,x+16,y + 40);
    };

    /* Punch action hit testerr */
    this.punch = function(context) {
      this.punched = false; 
      this.context = context;
      this.test = context.bounds.clone(); 
      this.test.w = 12;
      if (context.sprite.direction == 1) {
        this.test.x = this.test.x + 20;
      }

      this.punching = function(ref) {
         if (ref.punched) {
              ref.punched();
              AudioHelper.punch_hit.play();
          } 
      };

      this.update = function() {
        if (!this.punched && context.sprite.getFrame() == 2) {
            var self = this;
            game.hitTest(self.test).map(function(n) {
                if (n.id != self.context.id) {
                    self.punching(n);
                }
            });

            this.punched = true;
        }
      };
    };
}

function PlayerSprite(hair, shirt) {
    var sprite = this.sprite = {};
    this.sprites = [];
    this.sprites.push(new SpriteAnimator("static/game/../game/images/player_naked.png"));
    var hair = window.sprite.player.hair[hair];
    this.sprites.push(new SpriteAnimator(hair));
    var shirt = window.sprite.player.shirt[shirt];
    this.sprites.push(new SpriteAnimator(shirt));
    this.direction = 0;
    this.animating = false;
    var self = this;
    
    this.playAll = function(anim) {
        this.sprites.map(function(n) {
           n.play(anim); 
        });
    };
    this.getFrame = function() {
        return this.sprites[0].anim.current;
    };
    this.left = function() {
        this.direction = 0;
    };
    this.right = function() {
        this.direction = 1;
    };
    this.play = function(anim, fin) { 
      var toplay;
      (this.direction === 0) ? toplay = this.sprite[anim] : toplay = this.sprite[anim+"_right"];
          
      toplay.onDone = fin;
      this.playAll(toplay);
    };
    this.playOnce = function(context) {
      console.log("DONE!");
      this.play("stand");
    };
    this.draw = function(ctx, x, y) { 
      this.sprites.map(function(n) {
          n.draw(ctx, x, y);
      });

    }; 
    /* Sprite definitions */
    sprite.stand = { frames: [0, 0], row: 0};
    sprite.stand_right = { frames: [0, 0], row: 1};
    sprite.run = { frames: [1,4], row: 0}; 
    sprite.run_right = { frames: [1, 4], row: 1};
    sprite.punch = { frames: [5,7], row: 0, onDone: function() { self.playOnce(); }};
    sprite.punch_right = { frames: [5,7], row: 1, onDone: function() { self.playOnce(); }};

}  
