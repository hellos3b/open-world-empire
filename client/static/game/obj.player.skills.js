var playerSkills = {
   fire_1: function(player, dir, id) {
      var skill_id = 1;
      if (id == -1) {
          var net_obj = [];
          net_obj.push(networkManager.ACTION);
          net_obj.push(networkManager.ACTIONTYPE.SKILL);
          net_obj.push(player.id);
          net_obj.push(skill_id);
          net_obj.push((dir==-1) ? 0 : 1);
          net_obj = net_obj.concat(IntToByte(player.bounds.x));
          net_obj = net_obj.concat(IntToByte(player.bounds.y));
          console.log("[sending:]");
          console.log(net_obj);
          networkManager.ws.send(new Uint8Array(net_obj));
      } else {
        player.sprite.play("punch", function() { 
          player.sprite.play("stand"); 
          player.acting = false; 
        });

        game.effects.push(new blast.red(player));
        game.effects.push(new blast.projectile.fire_basic(player.bounds.x, player.bounds.y, dir, id, player.id));

        AudioHelper.fire.play();

      }
   },
   ice_wall: function(player, dir, id) {
       var skill_id = 2; 
      if (id == -1) {
          var net_obj = [];
          net_obj.push(networkManager.ACTION);
          net_obj.push(networkManager.ACTIONTYPE.SKILL);
          net_obj.push(player.id);
          net_obj.push(skill_id);
          net_obj.push((dir==-1) ? 0 : 1);
          net_obj = net_obj.concat(IntToByte(player.bounds.x));
          net_obj = net_obj.concat(IntToByte(player.bounds.y));
          console.log("[sending:]");
          console.log(net_obj);
          networkManager.ws.send(new Uint8Array(net_obj));
      } else {
        player.sprite.play("punch", function() { 
          player.sprite.play("stand"); 
          player.acting = false; 
        });

        game.effects.push(new blast.ice(player));
        game.gameObjects.push(new blast.projectile.ice_wall(player.bounds.x, player.bounds.y, dir, id, player.id));

        AudioHelper.fire.play();

      }
   },
   getSkill: function(id) {
        var self = this;
        var skills = {
            1: self.fire_1,
            2: self.ice_wall
        };

        return skills[id];
    }
};
