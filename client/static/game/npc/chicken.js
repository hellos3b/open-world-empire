function Chicken(x, y) {
   this.type = "NPC";
   this.id = -1;
   this.bounds = new Bounds(x, y, 32, 32);

   this.destination = null;
   this.speed = 3;

   this.sprite = new ChickenSprite();

   this.punched = function() {
      var self = this;
      var chickenpunch = new Uint8Array(4);
      chickenpunch[0] = networkManager.NPC;
      chickenpunch[1] = 1;
      chickenpunch[2] = this.id;
      networkManager.ws.send(chickenpunch);
      game.effects.push(new Hit(self.bounds.x + 4, self.bounds.y + 14, "0"));
    };
   this.walkTo = function(x, y) {
      this.destination = new Vec2(x, y);
      if (this.destination.x < this.bounds.x) {
          this.sprite.left();
      } else {
          this.sprite.right();
      }
      if (this.speed > 3) {
        AudioHelper.chicken.play();
      }
      this.sprite.play("walk");
    };
   this.Load = function() {};

   this.update = function() {
      if (this.destination) {
        var pos = new Vec2(this.bounds.x, this.bounds.y);   
        pos = this.destination.subV(pos);
        if (pos.length() < 10) {
            this.destination = null;
            this.sprite.play("stand");
        } else {
          pos.normalize();
          pos = pos.mulS(this.speed);
          this.bounds.x += pos.x;
          this.bounds.y += pos.y;
        }
      } else {
          if (Math.floor(Math.random() * 100) == 10) {
              this.sprite.play("peck");
          }
      }
    };

  this.draw = function(ctx) {
      var x = game.offset.offx(this.bounds.x);
      var y = game.offset.offy(this.bounds.y);
      this.sprite.draw(ctx, x, y);
  };
}


function ChickenSprite() {
   var sprite = this.sprite = new SpriteAnimator("static/game/images/animal_chicken.png");
   var anim = this.anim = {};
   var self = this;
   this.direction = 0; 

   this.left = function() { this.direction = 0; };
   this.right = function() { this.direction = 1; };
   this.play = function(anim) {
      var playanim = this.anim[anim];
     playanim.row = this.direction;
     this.sprite.play(playanim);
    };

  this.draw = function(ctx, x, y) {
      this.sprite.draw(ctx, x, y);
  };
   anim.stand = { frames: [0,0], row: 0};
   anim.walk = { frames: [1,2], row: 0};
   anim.peck = { frames: [3,8], row: 0, onDone: function() { self.play("stand"); }};
}
