function World() {
   this.data = [];   
   this.w = 32;
   this.h = 32;

   this.load = function() {
        var self = this;
        $.get("/getWorld", function(data) {
            self.data = JSON.parse(data);
        });

        for (var i in this.sprites) {
            var src = this.sprites[i];
            this.sprites[i] = new Image();
            this.sprites[i].src = src;
        }
    };
    this.getTile = function(x, y) {
        return this.data.filter(function(n) {
            return n.row == x;
        }).map(function(n) {
          return n.data.filter(function(i) {
              return i.id == y;
          });
        }).shift();
    };
    this.draw = function(ctx) {
       var h = this.h;
       var w = this.w;
       var sprites = this.sprites;
       this.data.map(function(n) {
           var x = n.row * w;
           n.data.map(function(t) {
              var y = t.id * h; 
              ctx.drawImage(sprites[1], game.offset.offx(x), game.offset.offy(y));
           });  
       });
    };
    
    this.sprites = {
        1: "static/game/images/world/grass.png",
        4: "static/game/images/world/grass.png"
    };
}
