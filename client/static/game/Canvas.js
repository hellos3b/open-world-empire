Canvas.init = function() {
    var Width = $(window).width();
    var Height = $(window).height();

    // create canvas object
    var mcanvas = this.mcanvas = document.createElement("canvas");
    mcanvas.id = "canvas";
    mcanvas.width = Width;
    mcanvas.height = Height;

    // append canvas
    var body = document.getElementsByTagName("body")[0];
    body.appendChild(mcanvas);

    // set html size
    var canvas = this.canvas = document.getElementById("canvas");
    $(this.canvas).width(Width)
        .height(Height);

    // adapt that shit son
    $(window).resize(Canvas.resize);

    // set context
    this.ctx = canvas.getContext('2d');
    this.ctx.font="11px Arial";
};

Canvas.resize = function() {
    var canvas = Canvas.canvas;
    var mcanvas = Canvas.mcanvas;

    mcanvas.width = $(window).width();
    mcanvas.height = $(window).height();

    $(canvas).css("display", "none");
    $(canvas).css({
        "width": $(window).width() + "px", 
        "height": $(window).height() + "px"
      }); 
    $(canvas).css("display", "block");
};

Canvas.clear = function() {
    var ctx = this.ctx;
    ctx.fillStyle = "#000000";
    ctx.fillRect(0,0,this.canvas.width,this.canvas.height);
};
