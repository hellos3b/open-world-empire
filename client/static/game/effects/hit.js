var Hit = function(x, y, points) {
   this.points = points;
   this.bounds = new Bounds(x, y, 0, 0);
   this.tick = 0;
   this.done = false;
   
   this.update = function() {
       this.tick++;
       
       if (this.tick > 30) {
            this.done = true;
        } 

        this.bounds.y -= 1;
    };

    this.draw = function(ctx) {
        var x = game.offset.offx(this.bounds.x);
        var y = game.offset.offy(this.bounds.y);
        ctx.fillStyle = "#FF0000";
        ctx.font="14px Arial";
        ctx.fillText(this.points, x, y);
    };
};
