blast.ice = function(p) {
   this.context = p; 
   this.bounds = new Bounds(-16, 0, 64, 32);
   this.sprite = new BlastIceSprite();
   this.done = false;  

   this.sprite.play("blow");
    this.update = function() {
        var self = this;
        this.done = this.sprite.done;
    };
    this.draw = function(ctx) {
      var x = game.offset.offx(this.context.bounds.x + this.bounds.x);
      var y = game.offset.offy(this.context.bounds.y +this.bounds.y);
      this.sprite.draw(ctx, x, y);
    };
};

function BlastIceSprite() {
   var sprite = this.sprite = new SpriteAnimator("static/game/images/effects/player_blast_blue.png");
   sprite.w = 64;
   sprite.fps = 2;

   var anim = this.anim = {};
   this.done = false;
   var self = this;

   this.play = function(anim) {
      var playanim = this.anim[anim];
      this.sprite.play(playanim);
    };

  this.draw = function(ctx, x, y) {
      this.sprite.draw(ctx, x, y);
  };
   anim.stop = { frames: [6,6], row: 0};
   anim.blow = { frames: [0,6], row: 0, onDone: function(){ self.play("stop"); self.done = true; }};

}

blast.projectile.ice_wall = function(sx, sy, dir, id, owner_id) {
   this.id = id;
   // physics
   this.solid = true;

   this.type = "Skill";
   this.ownerid = owner_id;
   console.log("OWNER: "+owner_id);
   this.sprite = new CastIceWall1();
   this.done = false;  
   this.direction = dir;
   this.bounds = new Bounds(sx + (32*dir), sy, 32, 32);
   this.tick = 0;
   this.sprite.play("grow");

    this.update = function() {
        var self = this;

        if (this.tick > 120) {
            this.done = true;
        }
        this.tick++;
    };

    this.collision = function() { this.done = true; };

    this.draw = function(ctx) {
      var x = game.offset.offx(this.bounds.x);
      var y = game.offset.offy(this.bounds.y);
      this.sprite.draw(ctx, x, y);
    };
};

function CastIceWall1() {
   var sprite = this.sprite = new SpriteAnimator("static/game/images/effects/ice_wall_1.png");
   sprite.fps = 2;

   var anim = this.anim = {};
   var self = this;

   this.play = function(anim) {
      var playanim = this.anim[anim];
      this.sprite.play(playanim);
    };

  this.draw = function(ctx, x, y) {
      this.sprite.draw(ctx, x, y);
  };

   anim.grow = { frames: [0,4], row: 0, onDone: function() { self.play("default"); }};
   anim.default = { frames: [4,4], row: 0};
}
