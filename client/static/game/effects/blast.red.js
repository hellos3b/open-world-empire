var blast={};

blast.red = function(p) {
   this.context = p; 
   this.bounds = new Bounds(-16, 0, 64, 32);
   this.sprite = new BlastRedSprite();
   this.done = false;  

   this.sprite.play("blow");
    this.update = function() {
        var self = this;
        this.done = this.sprite.done;
    };
    this.draw = function(ctx) {
      var x = game.offset.offx(this.context.bounds.x + this.bounds.x);
      var y = game.offset.offy(this.context.bounds.y +this.bounds.y);
      this.sprite.draw(ctx, x, y);
    };
};

function BlastRedSprite() {
   var sprite = this.sprite = new SpriteAnimator("static/game/images/effects/player_blast_red.png");
   sprite.w = 64;
   sprite.fps = 2;

   var anim = this.anim = {};
   this.done = false;
   var self = this;

   this.play = function(anim) {
      var playanim = this.anim[anim];
      this.sprite.play(playanim);
    };

  this.draw = function(ctx, x, y) {
      this.sprite.draw(ctx, x, y);
  };
   anim.stop = { frames: [6,6], row: 0};
   anim.blow = { frames: [0,6], row: 0, onDone: function(){ self.play("stop"); self.done = true; }};

}

blast.projectile = {};
blast.projectile.fire_basic = function(sx, sy, dir, id, owner_id) {
   this.id = id;
   this.type = "Skill";
   this.ownerid = owner_id;
   console.log("OWNER: "+owner_id);
   this.bounds = new Bounds(sx, sy, 32, 32);
   this.sprite = new BlastRedProjectileSprite();
   this.done = false;  
   this.direction = dir;
   this.speed = 6;
   this.tick = 0;
   this.sprite.play("default");

    this.update = function() {
        var self = this;
        this.bounds.x = this.bounds.x + this.speed * this.direction;

        if (this.tick > 120) {
            this.done = true;
        }
        this.tick++;
        var testBounds = new Bounds(this.bounds.x + 10, this.bounds.y + 10, 8, 8);
        game.gameObjects.filter(function(n) {
          return (n.type == "Player") ? 
              ((n.id == self.ownerid) ? false : n.bounds.intersects(testBounds))
              : false ;
        }).map(function(n) {
          self.collide(n);
          //self.done = true;
        });
    };

    this.collision = function() { this.done = true; };

    this.collide = function(ref) {
       console.log('blasted ' + ref.type);   
       if (networkManager.id == this.ownerid) 
           networkManager.checkCollision(this, ref);
//       game.effects.push(new Hit(ref.bounds.x + 8, ref.bounds.y + 8, 10));
    };
    this.draw = function(ctx) {
      var x = game.offset.offx(this.bounds.x);
      var y = game.offset.offy(this.bounds.y);
      this.sprite.draw(ctx, x, y);
    };
};

function BlastRedProjectileSprite() {
   var sprite = this.sprite = new SpriteAnimator("static/game/images/effects/fireball_1.png");
   sprite.fps = 2;

   var anim = this.anim = {};
   var self = this;

   this.play = function(anim) {
      var playanim = this.anim[anim];
      this.sprite.play(playanim);
    };

  this.draw = function(ctx, x, y) {
      this.sprite.draw(ctx, x, y);
  };
   anim.default = { frames: [0,3], row: 0};
}
