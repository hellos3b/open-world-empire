var sprite = {};

sprite.player = {
    load: function() {
      var b = this.body;
      this.body = new Image();
      this.body.src = b;

      for (var i = 0; i < this.hair.length; i++) {
          var h = this.hair[i];
          this.hair[i] = new Image();
          this.hair[i].src = h;
      }
      for (var i = 0; i < this.shirt.length; i++) {
          var h = this.shirt[i];
          this.shirt[i] = new Image();
          this.shirt[i].src = h;
      }
    },
    nextShirt: function(f) {
      if (f == this.shirt.length - 1 ) {
          return 0;
      } else {
        return f + 1;
      }
    },
    nextHair: function(f) {
      if (f == this.hair.length - 1 ) {
          return 0;
      } else {
        return f + 1;
      }
    },
    body: "static/game/images/player_naked.png",
    hair: [
       "static/game/images/player_hair_black.png",
       "static/game/images/player_hair_brown.png",
       "static/game/images/player_hair_red.png",
       "static/game/images/player_hair_white.png",
       "static/game/images/player_hair_bald_black.png",
       "static/game/images/player_hair_bald_brown.png", 
       "static/game/images/player_hair_bald_white.png"
    ],
    shirt: [
       "static/game/images/player_shirt_brown.png",
       "static/game/images/player_shirt_black.png",
       "static/game/images/player_shirt_mustard.png",
       "static/game/images/player_shirt_teal.png",
       "static/game/images/player_shirt_red.png"
    ]
};
