/* Add an entity to the game */
game.addEntity = function(properties) {
     var entity = new EntityObject();
     
     entity.addComponent(properties);

     this.entities.push( entity );

     return entity;
};

/* Next id */
game.nextId = function() { return game.ids++; };

/* Add, get drawable Component. Draw on each game loop */
game.addRenderer = function(renderer) { game.components.render.push(renderer); };
game.getRenderers = function() { return game.components.render; };

/* Add Physics objects. .update() on each game loop */
game.addUpdateComponent = function(component) { game.components.updates.push(component); }; 
game.getUpdateComponents = function() { return game.components.updates; };

/* Add colliders */
game.addCollider = function(component) { game.components.colliders.push(component); };
game.getColliders = function() { return game.components.colliders; };

/* Add Timer. .update() each game loop, remove when done */
game.addTimerComponent = function(component) { game.timers.push(component); };
game.updateTimerComponents = function() {
    game.timers = game.timers.map(function(n) {
          return n.update();       
      }).filter(function(n) {
          return !n.done;
      });
};
