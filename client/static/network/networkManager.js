var networkManager = {
    id: -1,
    tick: 0,
    ws: {},
    update: function() {
      this.tick++;
      if (this.tick == 60) {
        this.ping();
        this.tick = 0;
      }   
    },
    connect: function() {
      this.ws = new WebSocket('ws://localhost:1337', 'echo-protocol');
      this.ws.binaryType = "arraybuffer";
       
      this.ws.onmessage = function(msg) {
        if (typeof msg.data == "string") {
            networkManager.hub( JSON.parse(msg.data));
        } else {
          var bytearray = new Uint8Array(msg.data);
          networkManager.binaryHub(bytearray);
        }
      };
      
      this.ws.onopen = function() {
        networkManager.getID();
      };
    },
    binaryHub: function(msg) {
      var obj;
      if (msg[0] == this.INPUT) {
          gameHandler.movePlayer(msg);
      } else if (msg[0] == this.PING) {
          this.PingData.addResult(msg[1]);
      } else if (msg[0] == this.NPC) {
          obj = NetworkPackets.parse(msg);
          gameHandler.handleNPCUpdate(obj);
      } else if (msg[0] == this.SNAPSHOT) {
          obj = NetworkPackets.parse(msg);
          gameHandler.snapshot(obj);
      } else if (msg[0] == this.ACTION) {
        //action, type(1=skill), playerid, skill thing, skillid
          obj = NetworkPackets.parse(msg);
          gameHandler.handlePlayerAction(obj);
      } else if (msg[0] == this.DAMAGE) {
          gameHandler.damagePlayer(msg[1], msg[2]); 
          if (msg.length == 4)
             gameHandler.contact(msg[3]); // what caused it
      }
    },
    hub: function(msg) {
       switch(msg.action) {
            case "ID":
              console.log("You are user: "+msg.id);
              networkManager.id = msg.id;
              game.player.id = msg.id;
              
              msg.gameData.filter(function(n) {
                 return n.id != msg.id; 
              }).map(function(n) {
                   var p = gameHandler.addPlayer(n);
                   p.bounds.x = n.x;
                   p.bounds.y = n.y;
                });

              gameHandler.initNPCs(msg.NPC);

              this.ping();
            break;
            case "join":
              gameHandler.addPlayer(msg.data);
              break;
            case "drop":
              gameHandler.removePlayer(msg.id);
              break;
            case "speak":
              console.log("Player spoke!");
              messageHandler.inMsg(msg); 
              break;
            case "move":
              if (msg.fromid != this.id)
                gameHandler.movePlayer(msg);
              break;
        } 
    },
    getID: function() {
      this.send({ action: "getID", name: player_settings.name, hair: player_settings.hair, shirt: player_settings.shirt});
      
      setTimeout(function() {
          if (networkManager.id == -1) {
              networkManager.getID();
          }
      }, 1000);
    },
    checkCollision: function(obj1, obj2) {
      // collision, id-skill, id-player
      var packet = [];
      packet.push(this.COLLISION);    
      packet.push(obj1.id);
      packet.push(obj2.id);
      this.ws.send(new Uint8Array(packet));
    },
    collisionIds: {
        Player: 1,
        Skill: 2
    },
    PingData: {
      results: [0,0,0,0,0,0,0,0,0],
      addResult: function(tick) {
         var t = game.tick;
         if (t < tick) {
              tick -= 255;
         }
         this.results.splice(0, 1);
         this.results.push(t - tick); 
      },
      get: function() {
          var result = 0;
          var l = networkManager.PingData.results.length;
          for (var i = 0; i < l; i++) {
              result += this.results[i];
          }
          return Math.floor((result/l)*33);
      } 
    },
    ping: function() {
//      var update = new Uint8Array(4);
      var update = [];
      update.push(this.PING);
      update = update.concat(IntToByte(game.player.bounds.x));
      update = update.concat(IntToByte(game.player.bounds.y)); 
      update.push(game.tick);
      console.log("[PING]"+update);
      this.ws.send(new Uint8Array(update));
    },
    send: function(msg) {
      this.ws.send( JSON.stringify(msg) );
    },
    parse: function(binary) {
       var msg = {};
       switch (binary[0]) {
            case this.INPUT:
              msg ={
                  id: binary[1],
                  up: binary[2],
                  left: binary[3],
                  down: binary[4],
                  right: binary[5],
                  space: binary[6]
                };
              break;
        } 

        return msg;
    },
    // enums
    INPUT: 1,
    PING: 2,
    NPC: 3,
    SNAPSHOT: 4,
    ACTION: 5,
    ACTIONTYPE: {
        SKILL: 1
    },
    COLLISION: 6,
    DAMAGE: 7
};
