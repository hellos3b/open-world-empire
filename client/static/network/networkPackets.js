var NetworkPackets = {
    parse: function(binary) {
       var msg = {};
       switch(binary[0]) {
           case 1: // USER INPUT
              msg = {
                 id   : binary[1],
                 up   : binary[2],
                 left : binary[3],
                 down : binary[4],
                 right: binary[5],
                 space: binary[6] 
              };
              break;
           case 2: // PING DATA
              break;
           case 3: // NPC Update
             switch(binary[1]) {
                 case 0: // ANIMAL 
                    switch (binary[2]) {
                        case 0: // WALK
                          msg = { 
                              type  : "npc",
                              action: "walk",
                              id    : binary[3], 
                              x     : binary[4], 
                              y     : binary[5], 
                              speed : binary[6] 
                            };
                          break;
                        case 1: // ACTION 1
                          msg = { 
                              type  : "npc",
                              action: 1,
                              id    : binary[3]
                            };
                          break;
                    }
                 break;
              }
              break;
            case 4: // SNAPSHOT
              msg = [];

              for (var i = 1; i < binary.length; i++) {
                  if (binary[i] == 1) {
                      msg.push({
                          type: "Player", 
                          id: binary[i+1], 
                          x: ByteToInt([binary[i+2],binary[i+3],binary[i+4]]), 
                          y: ByteToInt([binary[i+5],binary[i+6],binary[i+7]])
                        });
                      i += 7;
                  } else if (binary[i] == 2) {
                      msg.push({
                            type: "npc",
                            id: binary[i+1], 
                            x: ByteToInt([binary[i+2],binary[i+3],binary[i+4]]), 
                            y: ByteToInt([binary[i+5],binary[i+6],binary[i+7]])
                          });
                      i += 7;
                  }
              }
              break;
            case 5: // ACTION
              switch (binary[1]) {
                  case 1: // SKILL
                    msg = {
                        action  : "skill",
                        id      : binary[2],
                        skill_type: binary[3],
                        dir     : binary[4],
                        skill_id: binary[5]
                    };
                   break;
              }
        } 


        return msg;
    }
};
function IntToByte(x) {
   var sign = 0;
   if (x < 0) { sign = 1; } 
   x = Math.abs(x);
   var r = 0;
   if (x !== 0) {
        r = Math.floor(x/256);
    }
    console.log(x+"-> "+[sign, x%256,r]);
   return [sign, x%256, r];
}
function ByteToInt(b) {
   var sign = 1;
   if (b[0] == 1) {
        sign = -1;
    }
   return sign*(b[1]+(b[2]*256));
}
