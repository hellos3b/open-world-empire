var count = 0;
var clients = {};


/* connect DB */
var databaseUrl = "world"; // "username:password@example.com/mydb"
var collections = ["World"];
var db = require("mongojs").connect(databaseUrl, collections);

/* connect http */
var http = require('http');

// Basic http server
var server = http.createServer(function(req, res) {
    switch (req.url) {
        case "/getWorld":
           res.writeHead(200);
           var world = db.collection('World');
           var result = world.find(function(err, docs) {
               res.end(JSON.stringify(docs));
            });
           //res.end(JSON.stringify({test: "hello"}));
           break;
        default:
          res.writeHead(200);
          res.end('success');
          break;
    }
});

// Just a start function, whatevs
server.listen(1337, function() {
    console.log((new Date()) + ' server is listening');
});

// Straight juice. websocket
var WebSocketServer = require('websocket').server;
wServer = new WebSocketServer({
    httpServer: server
});

// When a connection comes in
wServer.on('request', function(r) {
    // just go on and accept it bitch
    var connection = r.accept('echo-protocol', r.origin);
    var id = clientHandler.newClient(connection);
    console.log("[JOIN] Client: "+id+" has joined us.");

    // on message, go relay that der msg
    connection.on('message', function(msg) {
        messageHub(id, msg);
    });
   
   // why leave?? :( 
    connection.on('close', function(reasonCode, description) {
        clientHandler.removeClient(id);
        console.log("[QUIT] Client: "+id+" has parted :'(");
        console.log("-> "+description);
    });
  });

function messageHub(id, msg) {
    var action = clientHandler.actions;
    if (msg.type == 'utf8') {
      var data = msg.utf8Data;
      data = JSON.parse(data);

      switch (data.action) {
          case "getID":
            action.getData(id, data);
            break;
          case "setName":
            action.setName(id, data.name);
            break;
          case "speak":
            action.speak(id, data.msg);
            break;
          case "move":
            //action.move(id, data.position);
            break;
      }
    } else if (msg.type == 'binary') {
       var bdata = msg.binaryData;
       var type = bdata[0];
       switch (type) {
          case network.INPUT:
            action.move(id, bdata);
            break;
          case network.PING:
            action.ping(id, bdata);
            break;
          case network.NPC:
            action.updateNPC(id, bdata);
            break;
          case network.ACTION:
            action.action(id, bdata);
            break;
          case network.COLLISION:
            action.attack(bdata);
            break;
        }
    }
}

var clientHandler = {
    ids: 0,
    clients: [],
    newClient: function(connection) {
      this.ids++;
      this.clients.push(new Client(connection, this.ids));
      return this.ids;
    },
    removeClient: function(id) {
       this.clients = this.clients.filter(function(n) { return n.id != id; }); 
       this.messageAll({action: "drop", id: id});
    },
    getClientById: function(id) {
      return this.clients.filter(function(n) { return n.id == id; });  
    },
    getClientByName: function(name) {
      return this.clients.filter(function(n) { return n.name == name; });
    },
    messageClient: function(id, msg) {
      this.getClientById(id)
        .map(function(n) { n.send(msg); } );
    },
    messageClientBinary: function(id, msg) {
      this.getClientById(id)
        .map(function(n) { n.sendB(msg); } );
    },
    messageAll: function(msg) {
       this.clients.map(function(n) { n.send(msg); }); 
    },
    messageAllBinary: function(msg) {
       this.clients.map(function(n) { n.sendB(msg); }); 
    },
    actions: {
      snapshot: function(id) {
          var data = [];
          data.push(network.SNAPSHOT);
          // snapshot -- [type, [player, id, x, y] + [npc, id, x, y]]
          clientHandler.clients.map(function(n) {
              data.push(1);
              data.push(n.id);
              data = data.concat(toBytes(n.x));
              data = data.concat(toBytes(n.y));
          });
          game.NPC.map(function(n) {
              data.push(2);
              data.push(n.id);
              data = data.concat(toBytes(n.bounds.x));
              data = data.concat(toBytes(n.bounds.y));
          });

          var b = new Buffer(data);
          clientHandler.messageClientBinary(id, b);
      },
      getData: function(id, data) { 
        var player = {};
        clientHandler.getClientById(id).map(function(n) {
            n.shirt = data.shirt;
            n.hair = data.hair;
            n.name = data.name;
            player = n;

        });
        var c = clientHandler.clients.map(function(n) { return n.toJson(); });
        var npcs = game.getAllJson();

        var obj =  {action: "ID", id: id, gameData: c, NPC: npcs};
        clientHandler.messageClient(id, obj);
        clientHandler.messageAll({action: "join", data: player.toJson() });
        this.snapshot(id);
      },
      ping: function(id, data) {
        var x = BytesToInt([data[1],data[2],data[3]]);
        var y = BytesToInt([data[4],data[5],data[6]]);
        clientHandler.getClientById(id).map(function(n) {
            if (Math.abs(n.y - y) < 8 && Math.abs(n.x - x) < 8) {
              n.x = x;
              n.y = y;
            } else {
                console.log("inaccurate");
              console.log("in: "+x+","+y);
              console.log("current: "+n.x+","+n.y);
            }
            
            var pinger = new Buffer(2);
            pinger[0] = network.PING;
            pinger[1] = data[7];
            clientHandler.messageClientBinary(id, pinger);
        }); 
        this.snapshot(id);
      },
      setName: function(id, name) {
          clientHandler.getClientById(id).map(function(n) { n.setName(name); });
      },
      speak: function(id, msg) {
          var user = clientHandler.getClientById(id)
            .map(function(n) { return n.name; })
            .toString() || "DEFAULT";

          console.log("[CHAT] "+user+": "+msg);
          var obj = {
              action: "speak",
              fromid: id,
              msg: msg,
              text: user
            };
          clientHandler.messageAll(obj);
      },
      move: function(id, inputs) {
          clientHandler.getClientById(id).map(function(n) {
              // update client coords
              n.keyInput(inputs);
              inputs[1] = id;
              clientHandler.messageAllBinary(inputs);
          });
      },
      updateNPC: function(id, data) {
          game.updateNPC(data);
      },
      action: function(id, data) {
         switch (data[1]) {
              case network.ACTIONTYPE.SKILL:
                this.playerskill(id, data);
              break;
         } 
      },
      attack: function(data) {
         var player = clientHandler.getClientById(data[2]).shift();
         var result = game.checkAttack(data[1], player);   

         if (result) {
            var dmg = Math.floor(Math.random()*100) + 50;
            var packet = [];
            packet.push(network.DAMAGE);
            packet.push(player.id);
            packet.push(dmg);
            packet.push(data[1]); // src id
            clientHandler.messageAllBinary(new Buffer(packet));

         } else {
              console.log("YOUR COLLISION WAS DETERMINED A LIE");
          }
      },
      playerskill: function(id, data) {
        // SS object
         console.log(data);
         var skill_type = data[3];
         var dir = data[4];
         var x = BytesToInt([data[5], data[6], data[7]]);
         var y = BytesToInt([data[8], data[9], data[10]]);
         var skillid = game.emulateSkill(skill_type, dir, x, y);

         var packet = [];
         packet.push(network.ACTION);
         packet.push(network.ACTIONTYPE.SKILL);
         packet.push(id);
         packet.push(skill_type);
         packet.push(data[4]);
         packet.push(skillid);

         var p = new Buffer(packet);
         clientHandler.messageAllBinary(p); 

      }
    }
};

/* Game loop */
var Game = require('./game/Game.js');
var game = new Game.Game();
game.init();

setInterval(function() { 
    if (game.update()) {
      game.getUpdates().map(function(n) {
          clientHandler.messageAllBinary(n);
      });
    }
}, 33);

/* Client obj */
function Client(connection, id) {
    this.connection = connection;
    this.id = id;
    this.name = "";
    this.shirt = 0;
    this.hair = 0;
    
    // world coords
    this.x = 0;
    this.y = 0;

    this.setName = function(name) { 
      console.log("[GEN] "+this.name+" changed their name to "+name);
      this.name = name;
    };
    this.send = function(msg) {
        this.connection.sendUTF( JSON.stringify(msg) );
    };
    this.sendB = function(msg) {
        this.connection.sendBytes(msg);
    };

    this.keyInput = function(input) {
        if (input[2]) {
           this.y -= 3; 
        }
        if (input[3]) {
            this.x -= 3;
        }
        if (input[4]) {
            this.y += 3;
        }
        if (input[5]) {
            this.x += 3;
        }
    };
    this.toJson = function() {
        return {
            id: this.id,
            name: this.name,
            hair: this.hair,
            shirt: this.shirt,
            x: this.x,
            y: this.y
        };
    };
}
function BytesToInt(b) {
    var sign = 1;
    if (b[0] == 1) { sign = -1; };
    var r = b[1];
    var base = b[2]*256;
    return sign * (r + base);
}
function toBytes(x) {
   var sign = 0;
   if (x < 0) { sign = 1; }
   x = Math.abs(x);
  var r;
  (x != 0) ? r = Math.floor(x/256) : r = 0;
  return [sign, x%256, r];
}

var network = {
    INPUT: 1,
    PING: 2,
    NPC: 3,
    SNAPSHOT: 4,
    ACTION: 5,
    ACTIONTYPE: {
        SKILL: 1
    },
    COLLISION: 6,
    COLLISIONIDS: {
        1: "Player",
        2: "Skill"
    },
    DAMAGE: 7
};
