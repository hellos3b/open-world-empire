var Vector = require('../Vector.js').Geometry;
var Bounds = require('../Bounds.js').Bounds;

function Chicken(x, y) {
   this.id = 0;
   this.x = x;
   this.y = y; 
   this.bounds = new Bounds(x, y, 32, 32);
   this.hasData = null;

   this.destination = null;
   this.speed = 3;

   this.walkTo = function(x, y) {
      this.destination = new Vector(x, y);
    };

   this.action = function(data) {
        if (data[1] == 1) { // got punched
             var xDest = this.bounds.x + Math.floor(Math.random() * 250) - 125; 
             if (xDest < 20 || xDest > 480) {
                  xDest = 200;
              }
             var yDest = this.bounds.y + Math.floor(Math.random() * 100) - 50; 
             if (yDest < 20 || yDest > 400) {
                  yDest = 200;
              }
             this.destination = new Vector(xDest, yDest);
             this.speed = 6;
               
            this.createPacket();
        }
   };

   this.createPacket = function() {
     this.hasData = new Buffer(7);
     this.hasData[0] = 3; // NPC
     this.hasData[1] = 0; // IS ANIMAL
     this.hasData[2] = 0; // WALK TO
     this.hasData[3] = this.id;
     this.hasData[4] = this.destination.x;
     this.hasData[5] = this.destination.y;
     this.hasData[6] = this.speed;
   };

   this.update = function() {
      if (this.destination) {
        var pos = new Vector(this.bounds.x, this.bounds.y);   
        pos = this.destination.subV(pos);
        if (pos.length() < 10) {
            this.destination = null;
            this.speed = 3;
        } else {
          pos.normalize();
          pos = pos.mulS(this.speed);
          this.bounds.x += pos.x;
          this.bounds.y += pos.y;
        }
      } else {
          var walk = Math.floor(Math.random() * 100);
          if (walk == 25) {
             var xDest = this.bounds.x + Math.floor(Math.random() * 250) - 125; 
             if (xDest < 20 || xDest > 480) {
                  xDest = 200;
              }
             var yDest = this.bounds.y + Math.floor(Math.random() * 100) - 50; 
             if (yDest < 20 || yDest > 400) {
                  yDest = 200;
              }
             this.destination = new Vector(xDest, yDest);
           
             this.createPacket();
          }
      }
    };
   this.toJson = function() {
        return {type: "Chicken", id: this.id, x: this.bounds.x, y: this.bounds.y};
    };
}

module.exports.Chicken = Chicken;
