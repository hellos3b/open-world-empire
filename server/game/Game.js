var Chicken = require('./NPC/Chicken.js').Chicken;
var Bounds = require('./Bounds.js').Bounds;


var skills = {
    1: function(id, x, y, dir) {
       this.id = id;
       this.bounds = new Bounds(x, y, 32, 32);
       this.dir = dir;
       this.done = false;
       this.tick = 0;

       this.update = function() {
          this.bounds.x = this.bounds.x + (6*this.dir);            
          if (this.tick > 120) {
              this.done = true;
          }
          this.tick++;
       };

       this.collide = function() {
            this.done = true;
        }
    },
    2: function(id, x, y, dir) {
       this.id = id;
       this.bounds = new Bounds(x + (32*dir), y, 32, 32);
       this.dir = dir;
       this.done = false;
       this.tick = 0;

       this.update = function() {
          if (this.tick > 120) {
              this.done = true;
          }
          this.tick++;
       };

       this.collide = function() {
            this.done = true;
        }
    }
};

function Game() {
  this.NPC = [];
  this.gameObjects = [];
  this.ids = 0;
  this.updates = [];
  
  this.init = function() {
    this.addNPC(new Chicken(200, 100));
  };
  this.update = function() {
    var self = this;
    this.NPC.map(function(n) { 
      n.update(); 
      if (n.hasData) {
          self.updates.push(n.hasData);
          n.hasData = null;
      }
    });
    this.gameObjects = this.gameObjects.map(function(n) {
          n.update();
          return n;
      }).filter(function(n) { return !n.done; });

    if (this.updates.length > 0) {
        return true;
    } else {
        return false;
    }

    if (this.ids == 255) { this.ids = 0; }
  };
  this.checkAttack = function(skill, player) {
     var p = new Bounds(player.x-10, player.y-10, 52, 52);

      var s = this.gameObjects.filter(function(n) { return n.id == skill; })
      .map(function(n) {
          if (n.bounds.intersects(p)) {
              n.collide();
          }
      });
      return (s.length >0 )? true : false;
  };
  this.emulateSkill = function(skill_type, dir, x, y) {
    var id = this.ids++;  
    dir = (dir===0) ? -1 : 1;
    var s = skills[skill_type];
    var skill = new s(id, x, y, dir);

    this.gameObjects.push(skill);

    return id;
  };
  this.getUpdates = function() {
      var updates = this.updates;
      this.updates = [];
      return updates;
  };
  this.addNPC = function(obj) {
      var newNPC = obj;
      newNPC.id = this.ids++;
      this.NPC.push(newNPC); 
  };
  this.getNPCById = function(id) {
     return this.NPC.filter(function(n) { return n.id == id; });   
  };
  this.updateNPC = function(data) {
     var npc = this.getNPCById(data[2]); 
     npc.map(function(n) { n.action(data); });
  };
  this.getAllJson = function() {
      return this.NPC.map(function(n) {
          return n.toJson();
      });
  };
}

module.exports.Game = Game;
