function Bounds(x, y, w, h) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;

    this.intersects = function(b) {
        return (Math.abs(this.x - b.x) * 2 < (this.w + b.w)) &&
            (Math.abs(this.y - b.y) * 2 < (this.h + b.h));
    };
    this.clone = function() {
        return new Bounds(this.x, this.y, this.w, this.h);
    };
}

module.exports.Bounds = Bounds;
